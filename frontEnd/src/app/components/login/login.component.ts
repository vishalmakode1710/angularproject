import { Component, OnInit } from '@angular/core';
import { LoginUser } from 'src/app/models/login-user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginUser:LoginUser
  constructor(private userService:UserService) { 
    console.log("Within login form");
    this.loginUser=new LoginUser()
  }

  ngOnInit(): void {
    this.login();
  }
  login() {
    this.loginUser.name="vishal";
    this.loginUser.password="vi";
    this.userService.login(this.loginUser).subscribe(res=>{
    console.log(res);
    let jsonObject=JSON.stringify(res);
    let jsonToken=JSON.parse(jsonObject)
    console.log(`User Token After Login:::${jsonToken["Token"]}`);
    localStorage.setItem('userToken',jsonToken["Token"]);
    
    })
  }

}
