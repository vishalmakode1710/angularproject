import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:User
  constructor(private userService:UserService) { 
    console.log("within register componant");
    this.user=new User()
  }

  ngOnInit(): void {
    //this.registerUser();
  }
  registerUser() {
    // this.user.name="chtn";
    // this.user.password="ch";
    // this.user.location="bhopal";
    // this.user.email="c@gmail.com";
     this.user.isblocked=false;
     console.log(this.user);
    this.userService.RegisterUser(this.user).subscribe(res=>{
      if(res){
      console.log("Registration success....");
      Swal.fire(
        'User Registration',
        'Registration Success',
        'success'
      )
      }
      else{
        console.log("Registraion failed....");
        
      }
      
      
    })
  }

}
