import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
 constructor(private httpclient:HttpClient) {
  }
    getAllProducts():Observable<Product[]> {
      return this.httpclient.get<Product[]>('https://localhost:7080/api/User/GetAllProducts') 
    }
}
